/*
 * StringCheck.java
 *   作成	IT-College	2016
 *------------------------------------------------------------
 * Copyright(c) Rhizome Inc. All Rights Reserved.
 */
/*
 * ------------------------------<< 演習課題 >>----------------------------------
 * 下記のプログラムは、とある短文投稿サイトの投稿部分のプログラムです。
 * しかし、下記のプログラムをチェックしたところ、以下のようなバグが確認されました。
 *
 * �@：何も入力していなかったとしても投稿できてしまう
 * �A：何文字でも投稿できてしまう
 *
 * それを踏まえて、以下のような仕様へ変更することとなった。
 * その対応を行ってください。
 *
 * a：何も入力していなかった場合は、「何も入力されていません」と表示させ再度入力待ち状態にする。
 * 		※「いまなにしてる？」という文言も再度表示させる。
 * b：141文字以上の場合、「投稿文が長すぎます」と表示させ再度入力待ち状態にする。
 * c：スペースのみで入力されている場合、「何も入力されていません」と表示させ再度入力待ち状態にする。
 * d：両端にスペースがあった場合は、そのスペースを除去する。
 *
 * ------------------------------------------------------------------------------
 */


/**
 * 単文投稿を行うプログラムです。
 * @author rhizome
 *
 */
public class StringCheck {

	/**
	 * 単文投稿を行うプログラムのメインです。
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("いまなにしてる？");
		String str = inputString();
		System.out.println();
		System.out.println("↓↓↓↓↓↓　投稿されました　↓↓↓↓↓↓");
		System.out.println(str);
	}

	/**
	 * 単文を入力させるメソッドです。
	 * @return
	 */
	public static String inputString() {
		java.util.Scanner scanner = new java.util.Scanner(System.in);
		String str = scanner.nextLine();
		return str;
	}
}
